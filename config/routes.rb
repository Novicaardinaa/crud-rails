Rails.application.routes.draw do
  resources :payments
  resources :pockets
  resources :students
  resources :exams
  resources :teachers
  resources :reports
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
