class TeachersController < ApplicationController
  def new
  	@teacher = Teacher.new
  end

  def create
  	teacher = Teacher.new(resource_params)
  	teacher.save
  	flash[:notice] = 'New data has been created'
  	redirect_to teachers_path
  end

  def edit
  	@teacher = Teacher.find(params[:id])
  end

  def update
  	@teacher = Teacher.find(params[:id])
  	@teacher.update(resource_params)
  	flash[:notice] = 'The data has been updated'
  	redirect_to teachers_path(@teacher)
  end

  def destroy
  	@teacher = Teacher.find(params[:id])
  	@teacher.destroy
  	flash[:notice] = 'The data has been deleted'
  	redirect_to teachers_path
  end

  def index
  	@teachers = Teacher.all
  end

  def show
  	id = params[:id]
  	@teacher = Teacher.find(id)
  end

  private
  def resource_params
  	params.require(:teacher).permit(:nik, :name, :age, :rombel, :mapel)
  end
end
