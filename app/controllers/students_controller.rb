class StudentsController < ApplicationController
  def new
  	@student = Student.new
  end

  def create
  	student = Student.new(resource_params)
  	student.save
  	flash[:notice] = 'New data has been created'
  	redirect_to students_path
  end

  def edit
  	@student = Student.find(params[:id])
  end

  def update
  	@student = Student.find(params[:id])
  	@student.update(resource_params)
  	flash[:notice] = 'The data has been updated'
  	redirect_to students_path(@student)
  end

  def destroy
  	@student = Student.find(params[:id])
  	@student.destroy
  	flash[:notice] = 'The data has been deleted'
  	redirect_to students_path
  end

  def index
  	@students = Student.all
  end

  def show
  	id = params[:id]
  	@student = Student.find(id)
  end

  private
  def resource_params
  	params.require(:student).permit(:name, :username, :age, :rombel, :address, :city, :nik)
  end
end
