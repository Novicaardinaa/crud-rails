class PaymentsController < ApplicationController
  def new
  	@payment = Payment.new
  end

  def create
  	payment = Payment.new(resource_params)
  	payment.save
  	flash[:notice] = 'New data has been created'
  	redirect_to payments_path
  end

  def edit
  	@payment = Payment.find(params[:id])
  end

  def update
  	@payment = Payment.find(params[:id])
  	@payment.update(resource_params)
  	flash[:notice] = 'The data has been updated'
  	redirect_to payments_path(@payment)
  end

  def destroy
  	@payment = Payment.find(params[:id])
  	@payment.destroy
  	flash[:notice] = 'The data has been deleted'
  	redirect_to payments_path
  end

  def index
  	@payments = Payment.all
  end

  def show
  	id = params[:id]
  	@payment = Payment.find(id)
  end

  private
  def resource_params
  	params.require(:payment).permit(:id_transaction, :status, :upload)
  end
end
