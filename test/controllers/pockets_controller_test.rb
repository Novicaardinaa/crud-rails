require 'test_helper'

class PocketsControllerTest < ActionDispatch::IntegrationTest
  test "should get demo" do
    get pockets_demo_url
    assert_response :success
  end

  test "should get index" do
    get pockets_index_url
    assert_response :success
  end

end
