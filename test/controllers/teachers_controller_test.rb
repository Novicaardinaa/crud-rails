require 'test_helper'

class TeachersControllerTest < ActionDispatch::IntegrationTest
  test "should get demo" do
    get teachers_demo_url
    assert_response :success
  end

  test "should get index" do
    get teachers_index_url
    assert_response :success
  end

end
