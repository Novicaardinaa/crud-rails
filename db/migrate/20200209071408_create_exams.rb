class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title, null: false, limit: 50
      t.string :mapel, null: false, limit: 50
      t.integer :duration, default: 0
      t.float :nilai, default: 0
      t.string :status, default: 'Non-aktif'
      t.string :level, null:false
      t.integer :student_id , null:false

      t.timestamps
    end
  end
  def down
    drop_table = exams
  end
end
