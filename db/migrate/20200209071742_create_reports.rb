class CreateReports < ActiveRecord::Migration[6.0]
  def up
    create_table :reports do |t|
      t.string :title, null: false
      t.float :hasil, null: false
      t.string :mapel, null: false
      t.integer :teacher_id, null: false
      t.integer :student_id, null: false
      t.timestamp :date

      t.timestamps
    end
  end
  def down
    drop_table = reports
  end
end
