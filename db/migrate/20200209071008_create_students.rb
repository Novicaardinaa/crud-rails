class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :name, null: false, limit: 50
      t.string :username, null: false, limit: 20
      t.integer :age, null: false
      t.string :rombel, null: false
      t.text :address
      t.string :city, null: false
      t.string :nik, null: false

      t.timestamps
    end
  end

  def down
    drop_table = students
  end
end
