# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.create(name:'Novica Ardina',username:'novicaardinaa',age:18,rombel:'RPL X-1',address:'Desa Tenjoayu',city:'Cicurug',nik:123456789)
Exam.create(title:'UH 1',mapel:'Indonesia',duration:60,nilai:80,status:'Aktif',level:'Kelas X',student_id:1)
Teacher.create(nik:1234567890,name:'Alvin Andrian',age:22,rombel:'RPL X-1',mapel:'Indonesia')
Report.create(title:'Laporan Indonesia',hasil:98,mapel:'Indonesia',teacher_id:1,student_id:1, date:'2020-02-14')